package exercise;

import java.time.LocalDate;

class App {
    // BEGIN
    public static String buildList(String[] arrayToHtml) {
        if (arrayToHtml.length == 0) {
            return "";
        }

        StringBuilder strToHtml = new StringBuilder();
        strToHtml.append("<ul>" + "\n");
        for (String a : arrayToHtml) {
            strToHtml.append("  ").append("<li>").append(a).append("</li>" + "\n");
        }
        strToHtml.append("</ul>");
        return strToHtml.toString();
    }

    public static String getUsersByYear(String[][] users, int year) {
        StringBuilder userToHtml = new StringBuilder();
        userToHtml.append("<ul>" + "\n");
        boolean userWasAdded = false;
        for (String[] b : users) {
            int userYear = LocalDate.parse(b[1]).getYear();
            if (userYear == year) {
                userToHtml.append("  ").append("<li>").append(b[0]).append("</li>" + "\n");
                userWasAdded = true;
            }
        }
        userToHtml.append("</ul>");
        if (!userWasAdded) {
            return "";
        }
        return userToHtml.toString();
    }
}
// END

//    // Это дополнительная задача, которая выполняется по желанию.
//    public static String getYoungestUser(String[][] users, String date) throws Exception {
//        // BEGIN
//
//        // END
//    }
//}
