package exercise;

class Card {
    public static void printHiddenCard(String cardNumber, int starsCount) {
        // BEGIN
        String star = "*";
        String hiddenNum = star.repeat(starsCount) + cardNumber.substring(12, 16);

        System.out.println(hiddenNum);
        // END
    }
}
