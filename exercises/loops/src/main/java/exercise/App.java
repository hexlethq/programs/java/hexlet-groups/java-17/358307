package exercise;

class App {
    // BEGIN
    public static String getAbbreviation(String text) {
        String shortName = "";
        String[] split = text.split(" ");

        for (String word : split) {
            shortName += word.substring(0, 1);
        }
        return shortName.toUpperCase();
    }
    // END
}
