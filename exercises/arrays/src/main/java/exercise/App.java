package exercise;

class App {
    // BEGIN
    public static int[] reverse(int[] arr) {
        if (arr.length == 0) {
            return arr;
        }
        for (int i = 0; i < arr.length / 2; i++) {
            int temp = arr[i];
            arr[i] = arr[arr.length - i - 1];
            arr[arr.length - i - 1] = temp;
        } return arr;
    }
    public static int getIndexOfMaxNegative(int[] arr2) {
        int retNumIndex = -1;
        if (arr2.length == 0) {
            return retNumIndex;
        }
        int minValue = -999999999;

        for (int i = 0; i < arr2.length; i++) {
            if (arr2[i] < 0 && arr2[i] > minValue) {
                minValue = arr2[i];
                retNumIndex = i;
            }
        }
            return retNumIndex;
    }
    // END
}
