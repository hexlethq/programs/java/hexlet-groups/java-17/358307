# Массивы

## src/main/java/exercise/App.java

## Задачи

* Создайте публичный статический метод `reverse()`, который принимает в качестве аргумента массив целых чисел и возвращает новый массив целых чисел с элементами, расположенными в обратном порядке. Если передан пустой массив, метод должен вернуть пустой массив.

```java
int[] numbers = {1, 2, -6, 3, 8};
int[] result = App.reverse(numbers);
System.out.println(Arrays.toString(result)); // => [8, 3, -6, 2, 1]
```

* Создайте публичный статический метод `getIndexOfMaxNegative()`, который принимает в качестве аргумента массив целых чисел и возвращает индекс максимального отрицательного элемента массива. Если передан пустой массив или в массиве нет отрицательных элементов, метод должен вернуть -1. Если в массиве несколько одинаковых минимальных элементов, нужно вернуть индекс первого из них.

```java
int[] numbers1 = {};
App.getIndexOfMaxNegative(numbers1); // -1

int[] numbers2 = {1, 4, 3, 4, 5};
App.getIndexOfMaxNegative(numbers2); // -1

int[] numbers3 = {1, 4, -3, 4, -5};
App.getIndexOfMaxNegative(numbers3); // 2

int[] numbers4 = {1, -3, 5, 4, -3, -10};
App.getIndexOfMaxNegative(numbers4); // 1
```

## Самостоятельная работа

* Реализуйте публичный статический метод `flattenMatrix()`. Метод принимает в качестве аргумента матрицу целых чисел (двумерный массив) и записывает все её элементы в одномерный массив. Если на вход передана пустая матрица, метод должен вернуть пустой массив.

```java
int[][] matrix1 = new int[][];
int[] result1 = App.flattenMatrix(matrix1);
System.out.println(Arrays.toString(result1));
// => []

int[][] matrix2 = {
    {1, 2, 3},
    {4, 5, 6}
};
int[] result2 = App.flattenMatrix(matrix2);
System.out.println(Arrays.toString(result2));
// => [1, 2, 3, 4, 5, 6]
```
