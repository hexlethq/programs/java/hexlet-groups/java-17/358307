package exercise;

class App {
    // BEGIN
    public static int[] sort(int[] arrNoSort) {

        for (int i = 0; i < arrNoSort.length; i++) {
            for (int k = i; k < arrNoSort.length; k++) {
                if (arrNoSort[i] > arrNoSort[k]) {          //сравнить значения элементов с индексами i/k в массиве, если верно, то поменять местами значения
                    int temp1 = arrNoSort[i];               //записал значение и-того элемента
                    int temp2 = arrNoSort[k];               //записал значение к-того элемента
                    arrNoSort[k] = temp1;                   //перезаписал значение к-того на значение и-того
                    arrNoSort[i] = temp2;                   //тоже самое для и-того
                }
            }
        }
        return arrNoSort;
    }
    // END
}
