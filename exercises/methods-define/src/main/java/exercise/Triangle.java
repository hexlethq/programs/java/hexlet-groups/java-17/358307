package exercise;

class Triangle {
    // BEGIN
    public static double getSquare(int a, int b, int c) {
        double rad = c * Math.PI / 180;
        double triSq = ((a * b) / 2) * Math.sin(rad);

        return triSq;
    }
    public static void main(String[] args) {

        System.out.println(getSquare(4, 5, 45));
    }
    // END
}
