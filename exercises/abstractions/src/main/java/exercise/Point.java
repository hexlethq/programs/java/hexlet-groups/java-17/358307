package exercise;

import java.util.Arrays;

class Point {
    // BEGIN
    public static int[] makePoint(int x, int y) {
        int[] point = {x, y};
        return point;
    }

    public static int getX(int[] point) {
        return point[0];
    }

    public static int getY(int[] point) {
        return point[1];
    }

    public static String pointToString(int[] point) {
        return Arrays.toString(point).replace("[", "(").replace("]", ")");
    }

    public static int getQuadrant(int[] point) {
        if (getX(point) > 0 && getY(point) > 0) {
            return 1;
        }
        if (getX(point) < 0 && getY(point) > 0) {
            return 2;
        }
        if (getX(point) < 0 && getY(point) < 0) {
            return 3;
        }
        if (getX(point) > 0 && getY(point) < 0) {
            return 4;
        } else {
            return 0;
        }
    }

    public static int[] getSymmetricalPointByX(int[] point) {
        return makePoint(getX(point), -getY(point));
    }
    // END
}
