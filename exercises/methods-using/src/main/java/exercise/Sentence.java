package exercise;

class Sentence {
    public static void printSentence(String sentence) {
        // BEGIN
        String sent = sentence;
        char x = sent.charAt(sent.length() - 1);

        if (x == '!') {
            System.out.println(sent.toUpperCase());
        } else {
            System.out.println(sent.toLowerCase());
        }
        // END
    }
}
