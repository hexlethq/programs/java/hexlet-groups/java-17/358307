package exercise;

class Converter {
    // BEGIN
    public static int convert(int number, String type) {
        int res;

            if (type.equals("b")) {
                res = number * 1024;
                return res;
            } else if (type.equals("Kb")) {
                res = number / 1024;
                return res;
            } else {
                return 0;
            }
        }
        public static void main(String[] args) {
            System.out.println("10 Kb = " + convert(10, "b") + " b");
        }
        // END
}
