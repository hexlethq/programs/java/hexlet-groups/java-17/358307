package exercise;

class App {
    // BEGIN
    public static String getTypeOfTriangle(int side1, int side2, int side3) {
        String result;

        if ((side1 + side2) < side3 || (side2 + side3) < side1 || (side1 + side3) < side2) {
            result = "Треугольник не существует";
            return result;
        } else if (side1 != side2 && side1 != side3 && side2 != side3) {
            result = "Разносторонний";
            return result;
        } else if (side1 == side2 && side2 == side3) {
            result = "Равносторонний";
            return result;
        } else {
            result = "Равнобедренный";
            return result;
        }
    }
    // END
}
