package exercise;

import java.util.Arrays;

class App {
    // BEGIN
    public static int getIndexOfMaxNegative(int[] array) {
        int index = -1;
        if (array.length == 0) {
            return index;
        }

        int minValue = Integer.MIN_VALUE;
        for (int i = 0; i < array.length; i++) {
            if (array[i] < 0 && array[i] > minValue) {
                minValue = array[i];
                index = i;
            }
        } return index;
    }

    public static int[] getElementsLessAverage(int[] array2) {
        int[] arrayNew = new int[array2.length];

        if (array2.length == 0) {
            return arrayNew;
        }

        int sumElementArr = 0;
        for (int a : array2) {
            sumElementArr += a;
        }

        int average = sumElementArr / array2.length;
        int index = 0;
        for (int i = 0; i < array2.length; i++) {
            if (array2[i] <= average) {
                arrayNew[index] = array2[i];
                index++;
            }
        } return Arrays.copyOf(arrayNew, index);
    }
    // END
}
